package com.ef.dao;

import java.util.List;

public interface Dao<T> {
    void save(T t);
	void truncate();
	void update(List<T> t);
}
