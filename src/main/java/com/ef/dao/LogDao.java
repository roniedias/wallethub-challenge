package com.ef.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.ef.factory.ConnectionFactory;
import com.ef.model.Log;
import com.ef.vo.Request;

public class LogDao implements Dao<Log> {
	
	Connection connection = new ConnectionFactory().getConnection();
	
	
	@Override
	public void truncate() {
		Statement stmt;
		try {
			stmt = connection.createStatement();
			stmt.execute("TRUNCATE log");
			stmt.close();
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void save(Log log) {
		PreparedStatement stmt;
		try {
			stmt = this.connection.prepareStatement("insert into log(date, ip, request, status, user_agent)values(?,?,?,?,?)");
			stmt.setString(1, String.valueOf(log.getDate()));
			stmt.setString(2, log.getIp());
			stmt.setString(3, log.getRequest());
			stmt.setInt(4, log.getStatus());
			stmt.setString(5, log.getUserAgent());
			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override 
	public void update(List<Log> logs) {
		System.out.println("Updating log table. Please wait...");
		truncate();
		for(Log l : logs) {
			save(l);
		}
		System.out.println("Log table successfully updated!");
	}
	
	public List<Request> getRequestsInPeriod(String begin, String end) {
		List<Request> requests = new ArrayList<>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT ip, count(*) as count FROM log WHERE date between '" + begin + "' AND '" + end + "' GROUP BY ip ORDER BY count");
			while (rs.next()) {
				String ip = rs.getString("ip");
				int occurrence = rs.getInt("count");
				requests.add(new Request(ip, occurrence));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return requests;
	}	
	
	
	

}
