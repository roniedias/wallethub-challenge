package com.ef.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.ef.factory.ConnectionFactory;
import com.ef.model.Audit;

public class AuditDao implements Dao<Audit> {

	Connection connection = new ConnectionFactory().getConnection();

	public void save(Audit audit) {
		PreparedStatement stmt;
		try {
			stmt = this.connection.prepareStatement("insert into audit(info)values(?)");
			stmt.setString(1, audit.getInfo());
			stmt.execute();
			stmt.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void truncate() {
	}

	@Override
	public void update(List<Audit> t) {
	}

}
