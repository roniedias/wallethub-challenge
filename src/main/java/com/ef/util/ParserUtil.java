package com.ef.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ef.model.Log;

public class ParserUtil {
	
	private ParserUtil() {}
	
	public static void checkResearchArguments(String startDate, String duration, String threshold) {
				
		
			if(duration == null || !isValidDuration(duration)) {
				System.out.println("Invalid value for duration argument: " + duration);
				System.exit(1);
			}
		
		
		
			if(threshold == null || !isValidThreshold(threshold)) {
				System.out.println("Invalid value for threshold argument: " + threshold);
				System.exit(1);
			}
		
		
		
			if(startDate == null || !isValidStartDate(startDate)) {
				System.out.println("Invalid value for startDate argument: " + startDate);
				System.exit(1);
			}
		
		
 	}
	
	
	public static boolean isValidAccessLogPath(String accessLogPath) {
		return Files.exists(new File(accessLogPath).toPath());
	}
	
	private static boolean isValidDuration(String duration) {
		return duration.equalsIgnoreCase("hourly") || duration.equalsIgnoreCase("daily"); 
	}
	
	private static boolean isValidThreshold(String threshold) {
		try {
			Integer.parseInt(threshold);
			if(Integer.valueOf(threshold) <= 0) {
				return false;
			}
			return true;
		}
		catch (NumberFormatException e) {
			return false;
		}
	}
	
	private static boolean isValidStartDate(String startDate) {
		try {
			new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").parse(startDate);
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	
	public static String getPeriodAhead(String startDateStr, int hours) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
		Date startDate = null;
		try {
			startDate = formatter.parse(startDateStr);
		} catch (ParseException e) {
			System.out.println("Invalid date string format!");
			e.printStackTrace();
		}
		Calendar calendar = Calendar.getInstance();
	    calendar.setTime(startDate);
	    calendar.add(Calendar.HOUR_OF_DAY, hours);
	    Date oneOurAhead = calendar.getTime();
	    return formatter.format(oneOurAhead);
	}
	
	public static List<Log> getLogAsObj(String filePath) {
		List<Log> accessLogList = new ArrayList<>();
		BufferedReader reader;
		
		try {
			reader = new BufferedReader(new FileReader(filePath));
			String line = reader.readLine();
			while(line != null) {
				line = reader.readLine();
				if(line != null) {
					String[] pair = line.split("\\|", -1);
					if(pair.length == 5) {
						accessLogList.add(new Log(pair[0], pair[1], pair[2], Integer.valueOf(pair[3]), pair[4]));
					}
					else {
						System.out.println("Log file must contain the following data: Date, IP, Request, Status, User Agent (pipe delimited) on ALL lines");
						System.exit(1);
					}
				}
			}
			reader.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return accessLogList;
	}


}
