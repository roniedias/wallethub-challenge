package com.ef.model;

public class Audit {
	
	private String info;

	public Audit(String info) {
		this.info = info;
	}

	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
}
