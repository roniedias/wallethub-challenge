package com.ef.model;

public class Log {
	
	private String date;
	private String ip;
	private String request;
	private int status;
	private String userAgent;
	
	public Log() {}
	
	public Log(String date, String ip, String request, int status, String userAgent) {
		this.date = date;
		this.ip = ip;
		this.request = request;
		this.status = status;
		this.userAgent = userAgent;
	}

	
	public void setDate(String date) {
		this.date = date;
	}
	public String getDate() {
		return date;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getIp() {
		return ip;
	}

	public void setRequest(String request) {
		this.request = request;
	}
	public String getRequest() {
		return request;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public int getStatus() {
		return status;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public String getUserAgent() {
		return userAgent;
	}
	
}