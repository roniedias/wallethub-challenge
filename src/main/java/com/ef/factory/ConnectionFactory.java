package com.ef.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
	
	Connection connection = null;
	
	public Connection getConnection() {
		
        try {
            
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/wallethub?user=root&password=");
 
        } catch (SQLException e) {
             
            e.printStackTrace();
             
        }
        return connection;
    }
	
}
