package com.ef.vo;

public class Request {
	
	private String ip;
	private int occurrences;
	
	public Request(String ip, int occurrences) {
		this.ip = ip;
		this.occurrences = occurrences;
	}
	
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	
	public int getOccurrences() {
		return occurrences;
	}
	public void setOccurrences(int occurrences) {
		this.occurrences = occurrences;
	}

}
