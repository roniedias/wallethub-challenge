package com.ef;

import java.util.List;

import com.ef.dao.AuditDao;
import com.ef.dao.LogDao;
import com.ef.model.Audit;
import com.ef.util.ParserUtil;
import com.ef.vo.Request;

public class Parser {

	private String accessLog;
	private String startDate;
	private String duration;
	private String threshold;

	public static void main(String[] args) {
		
		Parser parser = new Parser(args);

		String accessLogArg = parser.getAccessLog();
		String startDate = parser.getStartDate();
		String duration = parser.getDuration();
		String threshold = parser.getThreshold();

		LogDao logDao;
		if (accessLogArg == null) {
			System.out.println("\"accessLog\" arg not informed. log table won't be updated");
			if (startDate == null || duration == null || threshold == null) {
				System.out.println("The three arguments are required: startDate, duration and threshold");
				System.exit(1);
			}
			ParserUtil.checkResearchArguments(startDate, duration, threshold);
			updateAuditTable(duration, startDate, threshold);
		} else {
			if (!ParserUtil.isValidAccessLogPath(accessLogArg)) {
				System.out.println("Invalid access log file path");
				System.exit(1);
			}
			if (startDate == null && duration == null && threshold == null) {
				logDao = new LogDao();
				System.out.println("Once \"startDate\" was a single arg informed, only log table will be updated");
				logDao = new LogDao();
				logDao.update(ParserUtil.getLogAsObj(accessLogArg));
				System.exit(0);
			} else {
				ParserUtil.checkResearchArguments(startDate, duration, threshold);
				System.out.println("All arguments provided. Both \"log\" and \"audit\" table will be updated");
				logDao = new LogDao();
				logDao.update(ParserUtil.getLogAsObj(accessLogArg));
				updateAuditTable(duration, startDate, threshold);
				System.exit(0);
			}

		}
	}

	public Parser(String[] args) {

		for (int i = 0; i < args.length; i++) {

			if (args[i] != null) {

				String[] entry = args[i].split("=");

				String key = entry[0];

				if (entry.length == 1 && !key.equalsIgnoreCase("accessLog")) {
					System.out.println("At least one value must be supplied for each argument");
					System.exit(1);
				}

				String value = entry[1];

				if (!key.startsWith("--")) {
					System.out.println("Please include \"--\" to your command line argument(s)");
					System.exit(1);
				} else {
					key = key.replaceFirst("--", "");
				}

				switch (key.toUpperCase()) {
				case "STARTDATE":
					this.startDate = value;
					break;
				case "ACCESSLOG":
					this.accessLog = value;
					break;
				case "DURATION":
					this.duration = value;
					break;
				case "THRESHOLD":
					this.threshold = value;
					break;
				default:
					System.out.println("Invalid argument supplied " + key);
				}
			}
		}
	}

	public String getAccessLog() {
		return accessLog;
	}

	public String getStartDate() {
		return startDate;
	}

	public String getDuration() {
		return duration;
	}

	public String getThreshold() {
		return threshold;
	}
	
	
	private static void updateAuditTable(String duration, String startDate, String threshold) {
		
		LogDao logDao = new LogDao();
		AuditDao auditDao = new AuditDao();
		
		int hours = 0;

		if (duration.equalsIgnoreCase("hourly")) {
			hours = 1;
		}
		if (duration.equalsIgnoreCase("daily")) {
			hours = 24;
		}

		String end = ParserUtil.getPeriodAhead(startDate, hours);
		List<Request> requests = logDao.getRequestsInPeriod(startDate, end);
		
		if(requests.size() == 0) {
			System.out.println("Log table is empty! please inform log file path by providing \"accessLog\" argument. Program will now be ended");
			System.exit(1);
		}
		
		for (Request r : requests) {
			if (r.getOccurrences() > Integer.valueOf(threshold)) {
				System.out.println(r.getIp());
				auditDao.save(new Audit("ip " + r.getIp() + " was blocked! it exceeded a threshold limit of "
						+ threshold + ". Total of " + r.getOccurrences() + " requests were made in " + hours
						+ " hour(s)."));
			}
		}

	}

}
