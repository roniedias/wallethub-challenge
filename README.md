**Assumptions:**

1. A MySQL server should be up and running before the program is executed;
2. The program also expects (before it's execution) that a database called "wallethub" is created, as as well as two tables: "log" and "audit". DDL commands are:


CREATE DATABASE IF NOT EXISTS wallethub;

CREATE TABLE IF NOT EXISTS log (
    date DATETIME,
    ip VARCHAR(255) NOT NULL,
    request VARCHAR(255) NOT NULL,
    status INT,
    user_agent TEXT NOT NULL
)  ENGINE=INNODB;


CREATE TABLE IF NOT EXISTS audit (
    info TEXT NOT NULL
)  ENGINE=INNODB;



**General informations:**

1. The MySQL connection configuration is available on ConnectionFactory.java class (package com.ef.factory). It's set to localhost, doesn't specify a port (mysql's default is 3306, so it's assumed), user "root" with empty password. Please refer to it, in case you desire to make changes (as IP, PORT, etc.);
2. To generate the executable java file (parser.jar), run the following maven command: "mvn clean compile assembly:single" from root project's folder. Resulting file will be available inside a folder named "target";


**Execution informations:**


1. If argument "accessLog=[path to access log]" is informed as a command line argument, log table will be truncated and updated with log file data;
2. If one of the arguments (startDate, duration, threshold) is provided, then ALL of them become required (if one of them is missing, then the program won't run);



**Examples of SQL commands (requested in the challenge):**

*Find IPs that mode more than a certain number of requests for a given time period:* 

Answer> SELECT result.ip from (SELECT ip, count(*) as count FROM log WHERE date between '2017-01-01.13:00:00' AND '2017-01-01.14:00:00' GROUP BY ip ORDER BY count) result where result.count > 100;



*Query to find requests made by a given IP:*

Answer> SELECT * FROM log WHERE ip = '192.168.169.194';
